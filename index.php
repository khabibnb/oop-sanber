<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");

echo "Name: ".$sheep->name."<br>"; // "shaun"
echo "Legs: ".$sheep->legs."<br>"; // 2
echo $sheep->cold_blooded."<br><br><br>"; // false

$kodok = new Frog("buduk");
echo "Nama: ".$kodok->name."<br>";
$kodok->jump() ; // "hop hop

$sungokong = new Ape("kera sakti");
echo "<br><br><br>Nama: ".$sungokong->name."<br>";
$sungokong->yell() // "Auooo";

?>